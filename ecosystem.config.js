module.exports = {
  apps: [{
    name: 'API',
    script: './dist/bundle.js',
    instances: 'max',
    env_development: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    },
    watch: false,
    max_memory_restart: '1G'
  }]
}
