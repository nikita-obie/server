#!/usr/bin/env node
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var express = _interopDefault(require('express'));
var createError = _interopDefault(require('http-errors'));
var logger = _interopDefault(require('morgan'));
var helmet = _interopDefault(require('helmet'));
var cors = _interopDefault(require('cors'));
var nodeMailer = _interopDefault(require('nodemailer'));
var http = _interopDefault(require('http'));

const path = require('path');
const app = express();

// Import .env environmental variables in development
require('dotenv').config();

// Whitelist Conduit domains
const whitelist = ['http://localhost:8080'];
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Request blocked due to CORS restrictions'));
    }
  },
  optionsSuccessStatus: 200
};

// Set up middleware
app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.options('*', cors());

// set up our API endpoint
app.post('/email', cors(corsOptions), function (req, res) {
  try {
    let transporter = nodeMailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'obiemortgagecalculator@gmail.com',
        pass: 'obiemortgageada1'
      }
    });

    let mailOptions = {
      to: req.body.email,
      subject: req.body.subject,
      text: `${req.body.message.payload}`
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error);
      }
      console.log(info.accepted, info.rejected);
    });
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(500);
  }
});

// catch 404s and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500)
    .json({
      error: err,
      details: err.message
    });
});

const debug = require('debug')('demo:server');

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || '8000');
app.set('port', port);

const server = http.createServer(app);

server.listen(port);
console.log('Now listening on port: ', port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 * @param {number} val - Port number 
 */

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 * @param {number} error - HTTP server error
 * @listens error
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
