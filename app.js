import express from 'express'
import createError from 'http-errors'
import logger from 'morgan'
import helmet from 'helmet'
import cors from 'cors'
import nodeMailer from 'nodemailer'

const path = require('path')
const app = express()

// Import .env environmental variables in development
require('dotenv').config()

// Whitelist Conduit domains
const whitelist = ['http://localhost:8080']
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Request blocked due to CORS restrictions'))
    }
  },
  optionsSuccessStatus: 200
}

// Set up middleware
app.use(helmet())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')))
app.use(logger('dev'))
app.options('*', cors())

// set up our API endpoint
app.post('/email', cors(corsOptions), function (req, res) {
  try {
    let transporter = nodeMailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: 'obiemortgagecalculator@gmail.com',
        pass: 'obiemortgageada1'
      }
    })

    let mailOptions = {
      to: req.body.email,
      subject: req.body.subject,
      text: `${req.body.message.payload}`
    }
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error)
      }
      console.log(info.accepted, info.rejected)
    })
    res.sendStatus(200)
  } catch (error) {
    res.sendStatus(500)
  }
})

// catch 404s and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
    .json({
      error: err,
      details: err.message
    })
})

export default app
