# API-Server 

> Served in development at: http://localhost:8000

## Introduction 

This README describes the internals of the OBie Server, which is meant to be used by Obie-produced front-ends, such as our website to email forms to users and presumably in the future to handle other user/admin operations. 

First off, let's talk directory structure: 

```md
.
    ├── app.js                              # Import app dependencies, setup middlewar, error-handler and email route
    ├── bin
    │   └── www                             # Setup ports and event listeners
    ├── dist
    │   └── bundle.js                       # Rollup's CommonJS output 
    ├── ecosystem.config.js                 # pm2 config file
    ├── package.json
    └── rollup.config.js
    
```

## Setup

### Prerequisites
In order to build the application, you will need the following:

* node.js version 10.13.0
* yarn package manager
* for MacOS:
  * xcode command line tools
* for Linux:
  * curl
  * git

### Usage
Now, install using yarn
``` bash
# install dependencies
yarn install

# bundle with hot reloading (so rollup bundles changes each time you save)
yarn watch 

# serve with hot reloading
yarn dev

# launch test runner
yarn test

# start the server for production using pm2 
yarn start

# lint all JS/Vue component files in 
yarn lint
```
