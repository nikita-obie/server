// rollup.config.js
import json from 'rollup-plugin-json'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import pkg from './package.json'

export default {
  input: 'bin/www',
  output: {
    file: 'dist/bundle.js',
    banner: '#!/usr/bin/env node',
    format: 'cjs'
  },
  external: Object.keys(pkg.dependencies),
  plugins: [
    resolve(),
    commonjs(),
    json()
  ]
}
